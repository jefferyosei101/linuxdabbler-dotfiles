#!/bin/bash

kbd_light_status="$(cat /sys/class/leds/tpacpi\:\:kbd_backlight/brightness)"
brightness_file="/sys/class/leds/tpacpi::kbd_backlight/brightness"

if [ $kbd_light_status = 0 ]; then
	echo "1" > $brightness_file
	echo 1
elif [ $kbd_light_status = 1 ]; then
	echo "2" > $brightness_file
	echo 2
else
	echo "0" > $brightness_file
	echo 0
fi

